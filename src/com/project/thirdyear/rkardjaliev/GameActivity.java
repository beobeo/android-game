package com.project.thirdyear.rkardjaliev;

import com.project.thirdyear.rkardjaliev.game.Controller;
import com.project.thirdyear.rkardjaliev.graphics.GLView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * This is the Game Activity class for the game. It sets up the {@link GLView
 * OpenGL Surface View} required for drawing the game and the {@link Controller
 * controller} and gets notified when push events occurr.
 * 
 * @author Robert Kardjaliev
 */
public class GameActivity extends Activity {
	/** Target Android SDK version. */
	private static final int DEFAULT_SDK_VERSION = 19;
	
	/** The OpenGL view. */
	private GLView mGlSurfaceView;

	/** An intance of the {@link Controller}. */
	private Controller controller;

	/** The top-level window decor view. */
	private View decorView;

	/**
	 * An alert dialog that requires user to press a button to remove.
	 */
	private AlertDialog.Builder alertDialogBuilder;

	/**
	 * A seek bar responsible for adjusting the shooting power in the game.
	 */
	private SeekBar seekBar;

	/** The shoot button. */
	private Button shootBtn;

	/**
	 * A boolean that tells the game whether it's single player or multiplayer.
	 */
	private boolean singlePlayer = false;

	/** The view where player 1's name is displayed. */
	private TextView pName1;
	/** The view where player 2's name is displayed. */
	private TextView pName2;
	/** The first player's name received from new game menu. */
	private String playerName1;
	/** The first player's name received from new game menu. */
	private String playerName2;
	/** The view where player 1's score is displayed. */
	private TextView score1;
	/** The view where player 2's score is displayed. */
	private TextView score2;
	/** The view where player 1's ball type is displayed. */
	private TextView ballType1;
	/** The view where player 2's ball type is displayed. */
	private TextView ballType2;

	/** An audio manager for setting sound volume. */
	private AudioManager audioManager;
	/** Sound volumes. */
	private float actVol, volume, maxVol;

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            The instance state.
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();
		playerName1 = intent.getStringExtra(NewGameMenu.PLAYER_ONE_NAME);
		playerName2 = intent.getStringExtra(NewGameMenu.PLAYER_TWO_NAME);
		singlePlayer = intent.getBooleanExtra(NewGameMenu.SINGLE_PLAYER,
				singlePlayer);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		decorView = getWindow().getDecorView();
		// hideSystemUI();
		audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		actVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		volume = actVol / maxVol;
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		alertDialogBuilder = new AlertDialog.Builder(this);
		setContentView(R.layout.game);
		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		mGlSurfaceView = new GLView(this, null);
		mGlSurfaceView = (GLView) findViewById(R.id.glsurfaceview);
		mGlSurfaceView.setPreserveEGLContextOnPause(true);
		controller = new Controller(this, this, mGlSurfaceView, playerName1,
				playerName2, volume, singlePlayer);
		shootBtn = (Button) findViewById(R.id.shootButton);
		shootBtn.setSoundEffectsEnabled(false);
		pName1 = (TextView) findViewById(R.id.player1);
		pName2 = (TextView) findViewById(R.id.player2);
		score1 = (TextView) findViewById(R.id.score1);
		score2 = (TextView) findViewById(R.id.score2);
		ballType1 = (TextView) findViewById(R.id.ballType1);
		ballType2 = (TextView) findViewById(R.id.ballType2);
		setNames(playerName1, playerName2);
		setScore(0, 0);
	}

	/**
	 * Hides navigation and action bars. Sets immersive full screen mode on.
	 */
	private void hideSystemUI() {
		if (android.os.Build.VERSION.SDK_INT >= DEFAULT_SDK_VERSION) {
			controller.setImmersive(true);
			decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		} else {
			controller.setImmersive(false);
			this.getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

	}

	/**
	 * Checks which window has the focus. If it's the decor view after the
	 * navigation bar has been shown, it sets the mode back to immersive full
	 * screen.
	 * 
	 * @param hasFocus
	 *            the boolean determining whether window has focus
	 */
	@Override
	public final void onWindowFocusChanged(final boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (hasFocus) {
			hideSystemUI();
		}
	}

	/**
	 * Pressing the back button from inside the game.
	 * 
	 * @param view
	 *            the view in which the button was pressed
	 */
	public final void backButton(final View view) {
		alertDialogBuilder.setTitle("Stop the Game?");
		alertDialogBuilder
				.setMessage("Click yes to exit!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
									final int id) {
								finish();
								Intent resumeMenuActivity = new Intent(
										GameActivity.this, MenuActivity.class);
								resumeMenuActivity.setFlags(
										Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
								startActivity(resumeMenuActivity);
							}
						})

				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int id) {

						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/**
	 * Game over.
	 * 
	 * @param name2
	 *            the name of the winning player
	 */
	public final void gameOver(final String name2) {
		alertDialogBuilder.setTitle("Game Over");
		alertDialogBuilder
				.setMessage(name2 + " wins.")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int id) {
						Intent resumeMenuActivity = new Intent(
								GameActivity.this, MenuActivity.class);
						resumeMenuActivity.setFlags(
								Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						startActivity(resumeMenuActivity);
						finish();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/**
	 * Pressing the shoot button from inside the game.
	 * 
	 * @param view
	 *            the view in which the button was pressed
	 */
	public final void shootButton(final View view) {
		controller.shoot(seekBar.getProgress());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public final void onBackPressed() {
		alertDialogBuilder.setTitle("Stop the Game?");
		alertDialogBuilder
				.setMessage(
						"All progress will be lost.\n Click yes to confirm!")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
									final int id) {
								finish();
								Intent resumeMenuActivity = new Intent(
										GameActivity.this, MenuActivity.class);
								resumeMenuActivity.setFlags(
										Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
								startActivity(resumeMenuActivity);
							}
						})

				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int id) {

						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected final void onResume() {
		super.onResume();
		this.mGlSurfaceView.onResume();
		hideSystemUI();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected final void onPause() {
		this.mGlSurfaceView.onPause();
		super.onPause();
	}

	/**
	 * Enable buttons.
	 */
	public final void enableButtons() {
		shootBtn.setEnabled(true);
		seekBar.setEnabled(true);
	}

	/**
	 * Disable buttons.
	 */
	public final void disableButtons() {
		shootBtn.setEnabled(false);
		seekBar.setEnabled(false);
	}

	/**
	 * Sets the names.
	 * 
	 * @param name1
	 *            the name1
	 * @param name2
	 *            the name2
	 */
	private void setNames(final String name1, final String name2) {
		pName1.setText(name1);
		pName2.setText(name2);
	}

	/**
	 * Sets the score.
	 * 
	 * @param i
	 *            the i
	 * @param j
	 *            the j
	 */
	public final void setScore(final int i, final int j) {
		if (!singlePlayer) {
		this.score1.setText("Score: " + i + "/7");
		this.score2.setText("Score: " + j + "/7");
		} else {
			this.score1.setText("Score: " + i + "/14");
			this.score2.setVisibility(TextView.INVISIBLE);
		}
	}

	/**
	 * Sets the ball types.
	 * 
	 * @param type1
	 *            player one's ball type
	 * @param type2
	 *            player two's ball type
	 */
	public final void setBallTypes(final String type1, final String type2) {
		if (!singlePlayer) {
		ballType1.setText(type1);
		ballType2.setText(type2);
		}
	}

	/**
	 * Gets called if player playing in single player mode
	 * scores black before before the rest.
	 */
	public final void lose() {
		alertDialogBuilder.setTitle("You Lose!");
		alertDialogBuilder
				.setMessage(
						playerName1 + " you potted the black ball!")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int id) {
						Intent resumeMenuActivity = new Intent(
								GameActivity.this, MenuActivity.class);
						resumeMenuActivity.setFlags(
								Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						startActivity(resumeMenuActivity);
						finish();
					}
				});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
