package com.project.thirdyear.rkardjaliev.models;


/**
 * The Pocket class. Holds the pockets
 * of the table to check for collisions with balls.
 * @author Robert Kardjaliev
 */
public class Pocket {

	/** The x coordinate. */
	public float xCoord;

	/** The y coordinate. */
	public float yCoord;

	/** The radius. */
	public float radius;

	/** The pocket's id. */
	public int number;

	/**
	 * Instantiates a new ball.
	 *
	 * @param id the pocket's id number
	 * @param pocketRadius the radius
	 * @param x the X coordinate
	 * @param y the Y coordinate
	 */
	public Pocket(final int id, final float pocketRadius, final float x, 
			final float y) {
		this.radius = pocketRadius;
		this.xCoord = x;
		this.yCoord = y;
		number = id;
	}

}