package com.project.thirdyear.rkardjaliev.game;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import com.project.thirdyear.rkardjaliev.GameActivity;
import com.project.thirdyear.rkardjaliev.R;
import com.project.thirdyear.rkardjaliev.graphics.GLView;
import com.project.thirdyear.rkardjaliev.models.Ball;
import com.project.thirdyear.rkardjaliev.models.BallType;

/**
 * The Class Controller. Responsible for implementing game rules.
 * 
 * @author Robert Kardjaliev
 */
public class Controller {
	/** Integer for handler class. */
	private static final int SET_TURN = 3;
	/** Integer for handler class. */
	private static final int WIN = 4;
	/** Integer for handler class. */
	private static final int LOSE = 5;
	/** Integer for handler class. */
	private static final int SCORE = 6;
	/** Integer for handler class. */
	private static final int BALL_IN_HAND = 7;

	/** The number of threads to be ran by the Sound Pool. */
	private static final int NUM_AUDIO_THREADS = 3;

	/** The current player(turn). */
	private Player currentPlayer;

	/** The other player(turn). */
	private Player otherPlayer;

	/** The first player. */
	private Player player1;

	/** The second player. */
	private Player player2;

	/** Max power percentage. */
	private static final int MAX_POWER = 100;

	/** The maximum score possible. */
	private static final int MAX_SCORE = 7;

	/** The power percentage. */
	private float power;

	/** An instance of the physics. */
	private Physics physics;

	/**
	 * The {@link com.project.thirdyear.rkardjaliev.GameActivity screen
	 * controller}.
	 */
	private GameActivity screenController;

	/** The current game state. */
	private GameState state = GameState.BEGIN;

	/**
	 * The bolean to check whether white ball should be reset.
	 */
	private boolean resetWhite = true;

	/**
	 * The {@link Handler} for sending message from GL thread to main UI thread.
	 */
	private Handler handler;

	/**
	 * A message passed from second thread to the main thread via handler.
	 */
	private Message msg;

	/** Boolean variable for initializing players' ball types. */
	private boolean firstPotted = false;
	/** The sound pool. */
	private SoundPool soundPool;

	/** A boolean to check whether a message has been displayed already. */
	private boolean messageShown = true;

	/** The integer id of the game sound. */
	private int ballCollisionSound = 0, wallCollisionSound = 0,
			pocketCollisionSound = 0, shootSound = 0;

	/** The volume. */
	private float volume;

	/** Boolean to determine whether it's a single player game. */
	private boolean singleplayer;

	/**
	 * Instantiates a new controller.
	 * 
	 * @param context
	 *            the context
	 * @param gameActivity
	 *            the game activity
	 * @param view
	 *            the OpenGL View
	 * @param pName1
	 *            the name of first currentPlayer
	 * @param pName2
	 *            the name of second currentPlayer
	 * @param mVolume
	 *            the system volume
	 * @param singlePlayer
	 *            the boolean showing whether it's multiplayer or not
	 */
	public Controller(final Context context, final GameActivity gameActivity,
			final GLView view, final String pName1, final String pName2,
			final float mVolume, final boolean singlePlayer) {
		this.singleplayer = singlePlayer;
		physics = new Physics(this);
		view.setMVC(this, physics);
		screenController = gameActivity;
		player1 = new Player(pName1);
		player2 = new Player(pName2);
		view.setMessage(player1.getName() + " breaks");
		setCurrentPlayer(player1);
		setOtherPlayer(player2);
		this.volume = mVolume;
		soundPool = new SoundPool(NUM_AUDIO_THREADS, AudioManager.STREAM_MUSIC,
				0);
		shootSound = soundPool.load(context, R.raw.shot, 0);
		ballCollisionSound = soundPool.load(context, R.raw.balls, 1);
		wallCollisionSound = soundPool.load(context, R.raw.wall, 1);
		pocketCollisionSound = soundPool.load(context, R.raw.pocket, 2);

		handler = new Handler() {
			@Override
			public void handleMessage(final Message message) {
				if (message.arg1 == 1) {
					screenController.enableButtons();
				}
				if (message.arg1 == 2) {
					screenController.setScore(player1.getScore(),
							player2.getScore());
				}
				if (message.arg2 == 2) {
					screenController.setBallTypes(player1.getBallType()
							.toString(), player2.getBallType().toString());
				}
				if (message.arg1 == SET_TURN) {
					view.setMessage(getCurrentPlayer().getName() + "'s turn");
				}
				if (message.arg1 == WIN) {
					screenController.gameOver(getOtherPlayer().getName());
				}
				if (message.arg1 == LOSE) {
					screenController.lose();
				}
				if (message.arg1 == SCORE) {
					screenController.setScore(player1.getScore(), 0);
				}
				if (message.arg1 == BALL_IN_HAND) {
					view.setMessage(getCurrentPlayer().getName()
							+ " has ball in hand.");
				}
			}
		};
	}

	/**
	 * Shoots the white ball, setting the game state as started, playing
	 * the shoot sound, disabling the seekbar and shoot button and changing
	 * the boolean variables holding game logic appropriately.
	 * 
	 * @param percentage
	 *            the percentage value of the seekbar
	 */
	public final void shoot(final int percentage) {
		int i = percentage;
		if (i < 2) {
			i = 2;
		}
		power = (float) i / MAX_POWER;
		physics.shoot(power);
		soundPool.play(shootSound, volume, volume, 0, 0, 1);
		setResetWhite(false);
		messageShown = false;
		getCurrentPlayer().setHasScored(false);
		getCurrentPlayer().setHasScoredWhite(false);
		setState(GameState.STARTED);
		disableButtons();
	}

	/**
	 * Disables the shoot button and seekbar.
	 */
	protected final void disableButtons() {
		screenController.disableButtons();

	}

	/**
	 * Enables the shoot button and seekbar.
	 */
	protected final void enableButtons() {
		msg = Message.obtain();
		msg.arg1 = 1;
		handler.sendMessage(msg);
	}

	/**
	 * Sends a toast message and signals that the white ball can be moved.
	 */
	public final void resetWhiteBall() {
		msg = Message.obtain();
		handler.sendMessage(msg);
		setResetWhite(true);
	}

	/**
	 * Sets the {@link GameState} to ENDED.
	 */
	public final void roundEnded() {
		setState(GameState.ENDED);
	}

	/**
	 * Gets the current {@link GameState state} of the game.
	 * 
	 * @return the game state
	 */
	public final GameState getState() {
		return state;
	}

	/**
	 * Sets the {@link GameState state} of the game.
	 * 
	 * @param gState
	 *            the new state
	 */
	public final void setState(final GameState gState) {
		this.state = gState;
	}

	/**
	 * Checks if the white ball should be reset.
	 * 
	 * @return true, if it is to be resetted and false, if not
	 */
	public final boolean isResetWhite() {
		return resetWhite;
	}

	/**
	 * Sets whether the white ball should be reset..
	 * 
	 * @param mResetWhite
	 *            the new reset white state
	 */
	public final void setResetWhite(final boolean mResetWhite) {
		this.resetWhite = mResetWhite;
	}

	/**
	 * Score.
	 * 
	 * @param ball
	 *            the ball
	 */
	public final void score(final Ball ball) {
		msg = Message.obtain();
		switch (ball.type) {
		case WHITE:
			if (!ball.enteredPocket) {
				ball.enteredPocket = true;
				getCurrentPlayer().setHasScoredWhite(true);
				playPocketCollisionSound();
			}
			break;
		case BLACK:
			if (!singleplayer) {
				if (getCurrentPlayer().getScore() == MAX_SCORE) {
					win();
				} else {
					changeTurn();
					win();
				}
			} else {
				if (player1.getScore() == MAX_SCORE * 2) {
					win();
				} else {
					lose();
				}
			}
			break;
		default:
			if (!firstPotted) {
				firstPotted = true;
				ball.enteredPocket = true;
				playPocketCollisionSound();
				if (!singleplayer) {
					msg.arg2 = 2;
					getCurrentPlayer().setBallType(ball.type);
					getOtherPlayer().setBallType(otherType(ball.type));
					handler.sendMessage(msg);
					msg = Message.obtain();
					msg.arg1 = 2;
					getCurrentPlayer().setHasScored(true);
					getCurrentPlayer().setScore(
							getCurrentPlayer().getScore() + 1);
					handler.sendMessage(msg);
				} else {
					player1.setScore(player1.getScore() + 1);
					msg.arg1 = SCORE;
					handler.sendMessage(msg);
				}
			} else if (!ball.enteredPocket) {
				ball.enteredPocket = true;
				playPocketCollisionSound();
				if (!singleplayer) {
					if (getCurrentPlayer().getBallType() == ball.type) {
						getCurrentPlayer().setHasScored(true);
						getCurrentPlayer().setScore(
								getCurrentPlayer().getScore() + 1);
					} else {
						getOtherPlayer().setScore(
								getOtherPlayer().getScore() + 1);
						changeTurn();
					}
					msg.arg1 = 2;
					handler.sendMessage(msg);
				} else {
					player1.setScore(player1.getScore() + 1);
					msg.arg1 = SCORE;
					handler.sendMessage(msg);
				}
			}
			break;
		}

	}

	/**
	 * Lose.
	 */
	private void lose() {
		msg = Message.obtain();
		msg.arg1 = LOSE;
		handler.sendMessage(msg);
		state = GameState.OVER;
	}

	/**
	 * Win.
	 * 
	 */
	private void win() {
		msg = Message.obtain();
		msg.arg1 = WIN;
		handler.sendMessage(msg);
		state = GameState.OVER;
	}

	/**
	 * Gets a {@link BallType.SOLID SOLID} or {@link BallType.STRIPE STRIPE} and
	 * returns the other one.
	 * 
	 * @param type
	 *            the type
	 * @return the ball type
	 */
	private BallType otherType(final BallType type) {
		if (type == BallType.SOLID) {
			return BallType.STRIPE;
		} else {
			return BallType.SOLID;
		}
	}

	/**
	 * Changes the current player's turn.
	 */
	final void changeTurn() {
		if (!singleplayer) {
			if (!getCurrentPlayer().hasScored()
					|| getCurrentPlayer().hasScoredWhite()) {
				if (player1.getTurn()) {
					setOtherPlayer(player1);
					setCurrentPlayer(player2);
				} else if (player2.getTurn()) {
					setOtherPlayer(player2);
					setCurrentPlayer(player1);
				}
				if (getOtherPlayer().hasScoredWhite()) {
					if (!messageShown) {
						messageShown = true;
						msg = Message.obtain();
						msg.arg1 = BALL_IN_HAND;
						handler.sendMessage(msg);
					}
				} else if (!messageShown) {
					messageShown = true;
					msg = Message.obtain();
					msg.arg1 = SET_TURN;
					handler.sendMessage(msg);
				}
			}
		} else if (getCurrentPlayer().hasScoredWhite()) {
			if (!messageShown) {
				messageShown = true;
				msg = Message.obtain();
				msg.arg1 = BALL_IN_HAND;
				handler.sendMessage(msg);
			}
		}
	}

	/**
	 * Gets the current player.
	 * 
	 * @return the current player
	 */
	private Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Sets the current player.
	 * 
	 * @param player
	 *            the new current player
	 */
	private void setCurrentPlayer(final Player player) {
		this.currentPlayer = player;
		currentPlayer.setTurn(true);
	}

	/**
	 * Gets the other player.
	 * 
	 * @return the other player
	 */
	public final Player getOtherPlayer() {
		return otherPlayer;
	}

	/**
	 * Sets the other player.
	 * 
	 * @param player
	 *            the new other player
	 */
	public final void setOtherPlayer(final Player player) {
		this.otherPlayer = player;
		player.setTurn(false);
	}

	/**
	 * Play wall collision sound.
	 */
	public final void playWallCollisionSound() {
		soundPool.play(wallCollisionSound, volume / 2, volume / 2, 1, 0, 1);
	}

	/**
	 * Play ball collision sound.
	 */
	public final void playBallCollisionSound() {
		soundPool.play(ballCollisionSound, volume, volume, 1, 0, 1);
	}

	/**
	 * Play pocket collision sound.
	 */
	public final void playPocketCollisionSound() {
		soundPool.play(pocketCollisionSound, volume, volume, 2, 0, 1);
	}

	/**
	 * Helps set the world size and scaling factors in physics based on whether
	 * it's immersive full screen or not.
	 * 
	 * @param b
	 *            the boolean value of whether game is in immersive mode
	 */
	public final void setImmersive(final boolean b) {
		physics.setImmersive(b);
	}
}
