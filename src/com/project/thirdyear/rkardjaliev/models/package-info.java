/**
 * The package for all the objects and their models.
 * 
 * @author Robert Kardjaliev
 * 
 * Using {@link Sphere} and related {@link Maths} to it from 
 * http://www.jimscosmos.com/code/android-open-gl-texture-mapped-spheres/
 * code available from 
 * https://docs.google.com/file/d/0By5c3cOVKbFpVTFpUlFlV2pGVWM/edit?pli=1
 * by author Jim Cornmell
 *
 */
package com.project.thirdyear.rkardjaliev.models;