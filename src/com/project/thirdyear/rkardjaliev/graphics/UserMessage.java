package com.project.thirdyear.rkardjaliev.graphics;

import com.project.thirdyear.rkardjaliev.R;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is a class for creating custom
 * Toast messages.
 * 
 * @author Robert Kardjaliev
 */
public class UserMessage extends View {
	
	/** An instance of a toast. */
	private Toast toast;
	
	/** The toast layout view. */
	private View layout;
	
	/** The text displayed in the Toast. */
	private TextView text;

	/**
	 * Instantiates a new user message.
	 *
	 * @param context the context
	 */
	public UserMessage(final Context context) {
		super(context);
		setupToast(context);
	}

	/**
	 * Set up the toast.
	 * 
	 * @param context
	 * 				the current application context
	 */
	private void setupToast(final Context context) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		layout = inflater.inflate(R.layout.toast_layout,
				(ViewGroup) super.findViewById(R.id.toast_layout_root));
		//layout.setRotation(90);
		text = (TextView) layout.findViewById(R.id.toasttext);
		toast = new Toast(context);
		//toast.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL, 0,  0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.setGravity(Gravity.BOTTOM | (Gravity.RIGHT), 180, 0);
		text.setTextSize(15);
	}
	
	/**
	 * Sets the message of the toast.
	 *
	 * @param s the new message
	 */
	public final void setMessage(final String s) {
		text.setText(s);
		toast.show();
	}
	/**
	 * Sets the message of the toast.
	 *
	 * @param s the new message
	 */
	public final void win(final String s) {
		text.setText(s);
		toast.setGravity(Gravity.FILL, 0, 0);
		text.setTextSize(50);
		text.setGravity(Gravity.CENTER);
		toast.show();
	}
}
