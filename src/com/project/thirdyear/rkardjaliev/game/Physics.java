package com.project.thirdyear.rkardjaliev.game;

import java.util.ArrayList;
import java.util.Iterator;

import com.project.thirdyear.rkardjaliev.models.Ball;
import com.project.thirdyear.rkardjaliev.models.BallType;
import com.project.thirdyear.rkardjaliev.models.Collisions;
import com.project.thirdyear.rkardjaliev.models.Pocket;
import com.project.thirdyear.rkardjaliev.models.Speed;
import com.project.thirdyear.rkardjaliev.models.Sphere;

/**
 * The game's physics class. Responsible for updating balls' models
 * appropriately.
 * 
 * @author Robert Kardjaliev
 * 
 */
public class Physics {

	/** An instance of the {@link Controller}. */
	private Controller controller;

	/** The balls' radius. */
	private static final float RADIUS = 0.8f;

	/** The large pockets' radius. */
	private static final float LARGE_POCKET_RADIUS = RADIUS * 1.4f;

	/** The small pockets' radius. */
	private static final float SMALL_POCKET_RADIUS = RADIUS * 1.1f;

	/** The list of all {@link Pocket pockets}. */
	private ArrayList<Pocket> pockets = new ArrayList<Pocket>();

	/** ArrayList containing all {@link Ball balls}. */
	private ArrayList<Ball> balls = new ArrayList<Ball>();

	/** A ball used for the aiming system. */
	private Ball aimBall;

	/** The white ball. */
	public Ball white;

	/** The angle used for shooting invisible aimBall. */
	private float aimAngle;

	/** Angle used for rotating stick image in Renderer. */
	private float stickAngle;

	/** 360 degrees constant. */
	public static final float THREE_SIXTY_DEGREES = 360;

	/** Ball depth for sphere texture generation. */
	public static final int BALL_DEPTH = 3;

	/** A string to carry messages for the log. */
	private static final String TAG = "PHYSICS";

	// speed limits
	/** The minimum speed at which balls will stop. */
	private static final float MINIMUM_SPEED = 0.009f;

	/** The maximum speed the white ball can be fired at. */
	private static final float MAXIMUM_SPEED = RADIUS;

	// friction
	/** The friction with the table surface. */
	private static final float FRICTION = 0.99f;

	/** The friction with the table walls. */
	private static final float WALL_FRICTION = 0.95f;
	
	/** The friction with the table walls. */
	private static final float UNSTICK_AMOUNT = 0.01f;

	// table walls
	/** The Constant TOP_WALL. */
	private static final float TOP_WALL = 13f;

	/** The Constant BOTTOM_WALL. */
	private static final float BOTTOM_WALL = -13f;

	/** The Constant LEFT_WALL. */
	private static final float LEFT_WALL = -29f;

	/** The Constant RIGHT_WALL. */
	private static final float RIGHT_WALL = 29f;

	// coordinate systems
	/** X coordinate of touch on screen. */
	public float touchX;

	/** Y coordinate of touch on screen. */
	public float touchY;

	/** Default World Height. */
	private static final float WORLD_HEIGHT = 40f;

	/** Default World Width. */
	private static final float WORLD_WIDTH = 64f;

	/**
	 * Scaling factor for X for screen to world coordinate transformation.
	 */
	private float scaleX;

	/**
	 * Scaling factor for X for screen to world coordinate transformation.
	 */
	private float scaleY;

	/**
	 * Float variable for calculating the sum of the radii squared for
	 * balls/pockets.
	 */
	private float sumRadiiSquared;

	/**
	 * Float variable for calculating distance between objects.
	 */
	private float distance;

	/**
	 * Float angle used in trigonometric functions.
	 */
	private float theta;

	/**
	 * A boolean variable to tell the Renderer whether to draw a stick or not.
	 */
	private boolean stickEnabled = true;

	/** The collisions. */
	private Collisions collisions;

	/** 
	 * Boolean to check whether screen is in default full screen mode
	 * or immersive to resize game appropriately.
	 */
	private boolean immersive;

	/**
	 * Boolean to keep track of whether a shot has been fired for
	 * changing turns and enabling the buttons when the balls stop moving.
	 */
	private boolean shotFired = false;

	/**
	 * Instantiates a Physics object.
	 * 
	 * @param mController
	 *            the controller
	 */
	public Physics(final Controller mController) {
		this.controller = mController;
		initPockets();
		initBalls();
		white = balls.get(0);
		collisions = new Collisions(balls.size());
	}

	/**
	 * Initializes the balls' locations.
	 */
	private void initBalls() {
		//CHECKSTYLE:OFF
		float initX = (RIGHT_WALL / 3) * 2; // X for first ball of rack
		float initY = 0; // Middle of table - for rack/cue ball
		initY -= RADIUS * 4;

		balls.add(new Ball(BallType.WHITE, 0, new Sphere(BALL_DEPTH, RADIUS),
				RADIUS, LEFT_WALL / 2, 0, new Speed(0, 0))); // cue ball

		int row = 0;
		int column = 0;
		int count = 5;
		for (int i = 1; i <= 15; i++) {
			if (column >= count) {
				column = 0;
				count--;
				row++;
			}
			balls.add(new Ball(BallType.NONE, i,
					new Sphere(BALL_DEPTH, RADIUS), RADIUS, initX
					- (RADIUS * row * 1.85f), initY + (row * RADIUS)
					+ (RADIUS * column * (2 + UNSTICK_AMOUNT)), new Speed(0f, 0f)));

			column++;
		}
		aimBall = new Ball(BallType.NONE, 199, new Sphere(BALL_DEPTH, RADIUS),
				RADIUS, LEFT_WALL / 2 - (2 * RADIUS), 0, new Speed(0, 0));
		//CHECKSTYLE:ON
	}

	/**
	 * Initializes the pockets.
	 * 
	 */
	private void initPockets() {
		//CHECKSTYLE:OFF
		pockets.add(new Pocket(0, LARGE_POCKET_RADIUS, LEFT_WALL, TOP_WALL));
		pockets.add(new Pocket(1, LARGE_POCKET_RADIUS, LEFT_WALL, BOTTOM_WALL));
		pockets.add(new Pocket(2, SMALL_POCKET_RADIUS, 0f, TOP_WALL + RADIUS));
		pockets.add(new Pocket(3, SMALL_POCKET_RADIUS, 0f, BOTTOM_WALL - RADIUS));
		pockets.add(new Pocket(4, LARGE_POCKET_RADIUS, RIGHT_WALL, TOP_WALL));
		pockets.add(new Pocket(5, LARGE_POCKET_RADIUS, RIGHT_WALL, BOTTOM_WALL));
		//CHECKSTYLE:ON
	}

	/**
	 * Checks whether a ball is in any of the pockets.
	 * 
	 * @param b
	 *            the ball
	 * @param p
	 *            the pocket being checked
	 * @return true if a ball is in a pocket.
	 */
	private boolean checkPocket(final Ball b, final Pocket p) {
		float distXSquared, distYSquared;
		if (!b.isAlive()) {
			return false;
		}
		distXSquared = p.xCoord - b.xCoord;
		distXSquared *= distXSquared;
		distYSquared = p.yCoord - b.yCoord;
		distYSquared *= distYSquared;
		distance = distXSquared + distYSquared;
		sumRadiiSquared = RADIUS + p.radius;
		sumRadiiSquared *= sumRadiiSquared;
		if (sumRadiiSquared < distance) {
			return false;
		}

		if (sumRadiiSquared >= distance) {
			return true;
		}
		return false;
	}

	/**
	 * Calls other methods to check for ball, wall, and pocket collisions.
	 * 
	 * @param ball1
	 *            a {@link Ball ball}
	 */
	public final void checkCollisions(final Ball ball1) {
		if (!areMoving()) { // if none of the balls are moving
			// skip all checks
			if (shotFired) {
				shotFired = false;
				controller.enableButtons();
				controller.changeTurn();
			}
			return;
		} else {
			for (Pocket p : pockets) {
				if (checkPocket(ball1, p)) {
					ball1.isInPocket(p.number);
					controller.score(ball1);
				}
			}
			if (checkWallCollisions(ball1) == 1) {
				controller.playWallCollisionSound();
			}
			int ballCollisions = 0;
			if (ball1.inPocket == -1) { // if first ball is in a pocket
				// skip checking it against other balls
				for (Ball ball2 : balls) {
					// if second ball isn't in a pocket and it's not the same
					// as first one
					if (ball2.inPocket == -1 && ball2.number != ball1.number) {
						// if the collision hasn't been checked before
						if (!collisions.collisionChecked(ball1.number,
								ball2.number)) {
							collisions.checkCollision(ball1.number,
									ball2.number);
							// if one of the balls is moving
							if (ball1.isMoving() || ball2.isMoving()) {
								ballCollisions = checkBallCollisions(ball2,
										ball1);
								// if they are colliding
								if (ballCollisions != 0) {
									controller.playBallCollisionSound();
									// if they are overlapping
									if (ballCollisions == 2) {
										unstick(ball2, ball1);
									}
									ballToBallResponse(ball2, ball1);
								}
							}
						}
					}
				}
			}
		}
		collisions.initCollisions();
	}

	/**
	 * Checks for ball to wall collisions.
	 * 
	 * @param ball
	 *            the ball
	 * @return 1 for wall collision, 2 for pocket, 0 otherwise
	 */
	public final int checkWallCollisions(final Ball ball) {
		// if ball not moving skip wall & pocket wall collision checks
		if (ball.inPocket == -1) {
			// wall collisions
			if (ball.xCoord + ball.radius >= RIGHT_WALL) {
				ball.xCoord = RIGHT_WALL - ball.radius;
				ball.speed.xVel *= -WALL_FRICTION;
				return 1;
			}
			if (ball.xCoord - ball.radius <= LEFT_WALL) {
				ball.xCoord = LEFT_WALL + ball.radius;
				ball.speed.xVel *= -WALL_FRICTION;
				return 1;
			}

			if (ball.yCoord + ball.radius >= TOP_WALL) {
				ball.yCoord = TOP_WALL - ball.radius;
				ball.speed.yVel *= -WALL_FRICTION;
				return 1;
			}

			if (ball.yCoord - ball.radius <= BOTTOM_WALL) {
				ball.yCoord = BOTTOM_WALL + ball.radius;
				ball.speed.yVel *= -WALL_FRICTION;
				return 1;
			}
		} else { // if ball is in pocket check for
			// pocket collisions
			float pX = pockets.get(ball.inPocket).xCoord;
			float pY = pockets.get(ball.inPocket).yCoord;
			float pRadius = pockets.get(ball.inPocket).radius;
			if (ball.xCoord + ball.radius >= pX + pRadius) {
				ball.xCoord = pX + pRadius - ball.radius;
				ball.speed.xVel *= -WALL_FRICTION;
				return 2;
			}
			if (ball.xCoord - ball.radius <= pX - pRadius) {
				ball.xCoord = pX - pRadius + ball.radius;
				ball.speed.xVel *= -WALL_FRICTION;
				return 2;
			}
			if (ball.yCoord - ball.radius <= pY - pRadius) {
				ball.yCoord = pY - pRadius + ball.radius;
				ball.speed.yVel *= -WALL_FRICTION;
				return 2;
			}
			if (ball.yCoord + ball.radius >= pY + pRadius) {
				ball.yCoord = pY + pRadius - ball.radius;
				ball.speed.yVel *= -WALL_FRICTION;
				return 2;
			}
		}
		return 0;
	}

	/**
	 * Checks for collisions between balls if they aren't in a pocket and if at
	 * least one of the balls is moving. It does so by checking if the sum of
	 * the radii is bigger than the distance between the centers of the balls.
	 * 
	 * @param ball1
	 *            the first ball
	 * @param ball2
	 *            the second ball
	 * @return an int determining whether balls are intersecting, touching, or
	 *         neither
	 */
	public final int checkBallCollisions(final Ball ball1, final Ball ball2) {
		float distXSquared, distYSquared;

		distXSquared = ball1.xCoord - ball2.xCoord;
		distXSquared *= distXSquared;
		distYSquared = ball1.yCoord - ball2.yCoord;
		distYSquared *= distYSquared;
		distance = distXSquared + distYSquared;
		sumRadiiSquared = (RADIUS * 2);
		sumRadiiSquared *= sumRadiiSquared;
		if (sumRadiiSquared < distance) {
			return 0; // if balls not touching skip rest
		}

		if (sumRadiiSquared == distance) { // balls touching
			ballToBallResponse(ball1, ball2);
			return 1;

		}

		if (sumRadiiSquared > distance) { // overlapping
			return 2;
		}
		return 0;
	}

	/**
	 * Ball unsticking method. If one of the balls isn't moving it sets the
	 * other ball relative to the center of the first +- the radius based on the
	 * arc tangent function. If both balls are moving sets them apart by the
	 * amount they're overlapping times the ratio of their velocity to the total
	 * velocity.
	 * 
	 * @param a
	 *            the first ball
	 * @param b
	 *            the second ball
	 */
	private void unstick(final Ball a, final Ball b) {
		theta = (float) Math.atan2(a.yCoord - b.yCoord, a.xCoord - b.xCoord);
		if (!a.isMoving()) {
			b.xCoord = (float) ((2 * RADIUS * Math.cos(theta)) + a.xCoord);
			b.yCoord = (float) ((2 * RADIUS * Math.sin(theta)) + a.yCoord);
		}
		if (!b.isMoving()) {
			a.xCoord = (float) ((2 * RADIUS * Math.cos(theta)) + b.xCoord);
			a.yCoord = (float) ((2 * RADIUS * Math.sin(theta)) + b.yCoord);
		}
		if (a.isMoving() && b.isMoving()) {
			float deltaDis = (float) Math.sqrt(sumRadiiSquared - distance);
			float x1, x2, y1, y2;
			x1 = Math.abs(a.speed.xVel);
			y1 = Math.abs(a.speed.yVel);
			x2 = Math.abs(b.speed.xVel);
			y2 = Math.abs(b.speed.yVel);
			float totalX = x1 + x2;
			float totalY = y1 + y2;
			a.xCoord -= a.speed.xVel * deltaDis * (x1 / totalX);
			a.yCoord -= a.speed.yVel * deltaDis * (y1 / totalY);
			b.xCoord -= b.speed.xVel * deltaDis * (x2 / totalX);
			b.yCoord -= b.speed.yVel * deltaDis * (y2 / totalY);

			/*
			 * Gets the difference between 2 * radius and the distance and moves
			 * them back by the ratio of their velocity to the total velocity,
			 * but it isn't working. 
			 * float overlapX = 2*RADIUS -
			 * Math.abs(a.xCoord - b.xCoord); float overlapY = 2*RADIUS -
			 * Math.abs(a.yCoord - b.yCoord); float totalX =
			 * Math.abs(a.speed.xVel) + Math.abs(b.speed.xVel); float totalY =
			 * Math.abs(b.speed.xVel) + Math.abs(b.speed.yVel); a.xCoord -=
			 * overlapX * (a.speed.xVel / totalX); a.yCoord -= overlapY *
			 * (a.speed.yVel / totalY); b.xCoord -= overlapX * (b.speed.xVel /
			 * totalX); b.yCoord -= overlapY * (b.speed.yVel / totalY);
			 */
		}
	}

	/**
	 * Checks if balls are moving.
	 * 
	 * @return true, if any of the balls are moving
	 */
	public final boolean areMoving() {
		for (Ball b : balls) {
			if (b.isMoving()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if balls are alive.
	 */
	public final void updateBallList() {
		Ball b;
		Iterator<Ball> ballIt = balls.iterator();
		if (!areMoving()) {
			while (ballIt.hasNext()) {
				b = ballIt.next();
				if ((!b.isAlive() || (b.inPocket != -1))) {
					if (b.type == BallType.WHITE) {
						b.setAlive();
						b.xCoord = LEFT_WALL / 2;
						b.yCoord = 0;
						b.speed.xVel = 0;
						b.speed.yVel = 0;
						white.enteredPocket = false;
						controller.resetWhiteBall();
					} else {
						ballIt.remove();
					}
				}
			}
		}
	}

	/**
	 * Ball to ball collision response.
	 * 
	 * @param ball1
	 *            first ball
	 * @param ball2
	 *            second ball
	 */
	public final void ballToBallResponse(final Ball ball1, final Ball ball2) {
		theta = (float) Math.atan2(ball1.yCoord - ball2.yCoord, ball1.xCoord
				- ball2.xCoord);
		float initVel1 = ball1.speed.getComponent(theta);
		float initVel2 = ball2.speed.getComponent(theta);

		float mass1 = 1;
		float mass2 = 1;

		float finalVel1 = ((mass1 - mass2) * initVel1 + 2 * mass2 * initVel2)
				/ (mass1 + mass2);
		float finalVel2 = ((mass2 - mass1) * initVel2 + 2 * mass1 * initVel1)
				/ (mass1 + mass2);

		ball1.speed.addComponent(theta, -initVel1 + finalVel1);
		ball2.speed.addComponent(theta, -initVel2 + finalVel2);
	}

	/**
	 * Method for updating ball positions and adding movement friction.
	 * 
	 * @param ball
	 *            the ball
	 */
	public final void move(final Ball ball) {
		if (ball.speed.xVel != 0 || ball.speed.yVel != 0) {
			ball.prevX = ball.xCoord;
			ball.prevY = ball.yCoord;
			ball.xCoord += ball.speed.xVel;
			ball.yCoord += ball.speed.yVel;

			if ((ball.speed.xVel <= MINIMUM_SPEED
					&& ball.speed.xVel >= -MINIMUM_SPEED)
					&& (ball.speed.yVel <= MINIMUM_SPEED
					&& ball.speed.yVel >= -MINIMUM_SPEED)) {
				ball.speed.xVel = 0;
				ball.speed.yVel = 0;
			}
			ball.speed.xVel *= FRICTION;
			ball.speed.yVel *= FRICTION;
		}
	}

	/**
	 * Adding ball rotation relative to their velocity.
	 * 
	 * @param ball
	 *            the {@link Ball}
	 */
	public final void spin(final Ball ball) {
		ball.xRotate += ball.speed.xVel * 50f;
		ball.yRotate += ball.speed.yVel * 50f;
		if (ball.xRotate - THREE_SIXTY_DEGREES >= 0
				|| ball.xRotate + THREE_SIXTY_DEGREES <= 0) { // no point in
			// keeping track of
			ball.xRotate %= THREE_SIXTY_DEGREES; // angles over 360
			// degrees
		}
		if (ball.yRotate - THREE_SIXTY_DEGREES >= 0
				|| ball.xRotate + THREE_SIXTY_DEGREES <= 0) {
			ball.yRotate %= THREE_SIXTY_DEGREES;
		}
	}

	/**
	 * Method that takes screen coordinates from touch event from GL Surface
	 * GLView and converts them to World Coordinates.
	 * 
	 * @param power
	 *            - the percentage of the maximum speed selected by user
	 */
	public final void shoot(final float power) {
		aimBall.xCoord = (float) (-(2 * RADIUS * Math.cos(aimAngle)) + balls
				.get(0).xCoord);
		aimBall.yCoord = (float) (-(2 * RADIUS * Math.sin(aimAngle)) + balls
				.get(0).yCoord);
		aimBall.speed.xVel = Math.signum(white.xCoord - aimBall.xCoord) * power
				* MAXIMUM_SPEED;
		aimBall.speed.yVel = Math.signum(white.yCoord - aimBall.yCoord) * power
				* MAXIMUM_SPEED;
		ballToBallResponse(aimBall, balls.get(0));
		shotFired = true;
	}

	/**
	 * Sets the rotation angle of the stick.
	 * 
	 * @param ang
	 *            the arbitrary angle by which the cue stick was rotated
	 */
	public final void setAimAngle(final float ang) {
		stickAngle = ang;
		aimAngle = ang;
		aimAngle *= (Math.PI / 180);
	}

	/**
	 * Returns the rotation aimAngle of the triangle shape (mTriangle).
	 * 
	 * @return - A float representing the rotation aimAngle.
	 */
	public final float getStickAngle() {
		return stickAngle;
	}

	/**
	 * Converts a touch event's coordinates from screen to world ones.
	 * 
	 * @param x
	 *            the X coordinate of the touch
	 * @param y
	 *            the Y coordinate of the touch
	 */
	public final void screenToWorld(final float x, final float y) {
		if (!immersive) {
			touchX = (x * scaleX) - ((WORLD_WIDTH + 4) / 2);
		}
		else {
			touchX = (x * scaleX) - (WORLD_WIDTH / 2);
		}
		touchY = (y * scaleY) + (WORLD_HEIGHT / 2);
	}

	/**
	 * Sets the scaling factor to convert screen coordinates to world ones.
	 * 
	 * @param width
	 *            the screen's width
	 * @param height
	 *            the screen's height
	 */
	public final void setScalingFactors(final int width, final int height) {
		if (!immersive) {
			scaleX = (WORLD_WIDTH + 4) / width;
		} else {
			scaleX = WORLD_WIDTH / width;
		}
		scaleY = WORLD_HEIGHT / -height;
	}

	/**
	 * Gets the list of all balls.
	 * 
	 * @return the list of {@link Ball balls}
	 */
	public final ArrayList<Ball> getBalls() {
		return balls;
	}

	/**
	 * Method for setting white ball position at start of the game or after it's
	 * gone in a pocket.
	 */
	public final void setWhite() {
		white.xCoord = touchX;
		white.yCoord = touchY;
		if (controller.getState() == GameState.BEGIN
				&& white.xCoord > LEFT_WALL / 2) {
			white.xCoord = LEFT_WALL / 2;
		}
		if (controller.getState() != GameState.BEGIN) {
			for (Ball b : balls) {
				if (checkBallCollisions(white, b) == 2) {
					theta = (float) Math.atan2(white.yCoord - b.yCoord,
							white.xCoord - b.xCoord);
					white.xCoord = (float) ((2 * RADIUS
							* Math.cos(theta)) + b.xCoord);
					white.yCoord = (float) ((2 * RADIUS
							* Math.sin(theta)) + b.yCoord);
				}
			}
		}
		checkWallCollisions(white);
		for (Pocket p : pockets) {
			if (checkPocket(white, p)) {
				theta = (float) Math.atan2(white.yCoord - p.yCoord,
						white.xCoord - p.xCoord);
				white.yCoord = (float) (-(Math.signum(p.yCoord)
						* (white.radius + p.radius)
						+ 0.05f * Math.sin(theta)) + p.yCoord);
			}
		}

	}

	/**
	 * Method for checking if the player selected the white ball.
	 * 
	 * @return true if the person has clicked within twice the ball radius
	 */
	public final boolean whiteSelected() {
		if ((touchX >= white.xCoord - 2 * white.radius)
				&& (touchX <= white.xCoord + 2 * white.radius)
				&& (touchY >= white.yCoord - 2 * white.radius)
				&& (touchY <= white.yCoord + 2 * white.radius)) {
			enableStick(false);
			return true;
		}
		return false;
	}

	/**
	 * Checks if stick is enabled.
	 * 
	 * @return the boolean stickEnabled
	 */
	public final boolean getStickEnabled() {
		return stickEnabled;
	}

	/**
	 * Method for enabling/disabling the stick from the.
	 * 
	 * @param b
	 *            the variable for setting the stick to enabled/disabled
	 */
	public final void enableStick(final Boolean b) {
		stickEnabled = b;
	}

	/**
	 * Sets the variable immersive according to the full screen mode
	 * for screen resizing.
	 * @param b
	 * 			the boolean holding the true or false value
	 */
	public final void setImmersive(final boolean b) {
		immersive = b;		
	}
}