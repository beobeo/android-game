package com.project.thirdyear.rkardjaliev;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioButton;

/**
 * The New Game Menu activity gets called after the player presses
 * new game in the Menu activity. Allows for single or multiplayer to
 * be selected and players' names to be entered. Starts the Game Activity.
 */
public class NewGameMenu extends Activity {

	/** A String for putting extra in intent. */
	public static final String PLAYER_ONE_NAME = "Player 1";
	/** A String for putting extra in intent. */
	public static final String PLAYER_TWO_NAME = "Player 2";
	/** A String for putting extra in intent. */
	public static final String SINGLE_PLAYER = "SINGLE PLAYER";

	/** The text field for player 1 to enter his name. */
	private EditText player1Field;
	/** The text field for player 1 to enter his name. */
	private EditText player2Field;

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            The instance state.
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_game_menu);
	}

	/**
	 * Called when the single player radio button is clicked.
	 * Hides the player 2 field.
	 * @param view the view associated with the radio button
	 */
	public final void singlePlayer(final View view) {
		player2Field = (EditText) findViewById(R.id.player2);
		player2Field.setVisibility(EditText.INVISIBLE);
	}

	/**
	 * Called when the mutliplayer radio button is clicked.
	 * Shows the player 2 field.
	 * @param view the view associated with the radio button
	 */
	public final void multiplayer(final View view) {
		player2Field = (EditText) findViewById(R.id.player2);
		player2Field.setVisibility(EditText.VISIBLE);
	}

	/**
	 * Called when the Begin button is pressed. Takes the names
	 * and player mode from the radio buttons, puts them into the
	 * intent and starts a new game with it.
	 * @param view the view associated with the button
	 */
	public final void startGame(final View view) {
		Intent intent = new Intent(this, GameActivity.class);
		player1Field = (EditText) findViewById(R.id.player1);
		player2Field = (EditText) findViewById(R.id.player2);
		String message = "Player one";
		if (player1Field.getText().toString().length() > 0) {
			message = player1Field.getText().toString();
		}

		String message2 = "Player two";
		if (player2Field.getText().toString().length() > 0) {
			message2 = player2Field.getText().toString();
		}
		player2Field.getText().toString();
		RadioButton single = (RadioButton) findViewById(R.id.singlePlayer);
		if (!single.isChecked()) {
			intent.putExtra(SINGLE_PLAYER, false);
			intent.putExtra(PLAYER_TWO_NAME, message2);
		} else {
			intent.putExtra(SINGLE_PLAYER, true);
		}
		intent.putExtra(PLAYER_ONE_NAME, message);
		startActivity(intent);
		finish();
	}
}
