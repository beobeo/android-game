package com.project.thirdyear.rkardjaliev.models;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

/**
 * A square model drawn using 2 triangle strips. Used for drawing the pool table
 * and cue stick.
 * 
 * @author Robert Kardjaliev
 */
public class Square {
	/** Buffer holding the vertices. */
	private FloatBuffer vertexBuffer;
	/** Model vertices. */
	private float[] vertices = { -1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 0.0f, 1.0f,
			-1.0f, 0.0f, 1.0f, 1.0f, 0.0f };
	/** Buffer holding the texture coordinates. */
	private FloatBuffer textureBuffer;
	/** Mapping coordinates for the vertices. */
	private float[] texture = { 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f};

	/** The texture pointer. */
	private int[] textures = new int[1];

	/**
	 * Instantiates a new square.
	 */
	public Square() {
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		vertexBuffer = byteBuffer.asFloatBuffer();
		vertexBuffer.put(vertices);
		vertexBuffer.position(0);

		byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		textureBuffer = byteBuffer.asFloatBuffer();
		textureBuffer.put(texture);
		textureBuffer.position(0);
	}

	/**
	 * Load the texture for the square.
	 * 
	 * @param gl
	 *            the {@link GL10} context
	 * @param context
	 *            the context
	 * @param id
	 *            the id
	 */
	public final void loadGLTexture(final GL10 gl, final Context context,
			final int id) {
		Bitmap bitmap = BitmapFactory
				.decodeResource(context.getResources(), id);

		gl.glGenTextures(1, textures, 0);
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,
				GL10.GL_LINEAR);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,
				GL10.GL_LINEAR);
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		bitmap.recycle();
	}

	/**
	 * The draw method for a square.
	 * 
	 * @param gl
	 *            the {@link GL10} context
	 */
	public final void draw(final GL10 gl) {
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glFrontFace(GL10.GL_CW);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
	}
}
