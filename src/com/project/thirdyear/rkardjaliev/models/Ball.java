package com.project.thirdyear.rkardjaliev.models;

import android.util.Log;

/**
 * The Ball class. Contains all the variables required to manipulate a ball
 * object.
 * 
 * @author Robert Kardjaliev
 */
public class Ball {
	/**
	 * A Z-axis velocity a ball gets when it
	 * enters a pocket.
	 */
    private static final float FALL_VELOCITY = 0.0002f;
    // Public and not using getters and setters
    // because most often used class
    // for calculations. Accessing fields directly is
    // 3 to 7 times faster in Android.
    /** The x coordinate. */
    public float xCoord;

    /** The y coordinate. */
    public float yCoord;

    /** The Z coordinate. Distance of ball from screen. */
    public float zCoord = -48.0f;

    /** Saves previous x coordinate. */
    public float prevX;
    /** Saves previous y coordinate. */
    public float prevY;

    /** An instance of a {@link Sphere} texture. */
    public Sphere sphere;

    /** The radius of the ball. */
    public float radius;

    /** The ball's X and Y velocity. */
    public Speed speed;

    /** Angle of rotation around X axis. */
    public float xRotate = 0f;

    /** Angle of rotation around Y axis. 90 degrees - balls facing up. */
    public float yRotate = 90f;

    /** The z rotate. */
    public float zRotate = 0f;

    /** The animation count for ball moving along Z axis. */
    private int zCount = 360;

    /** The alive. */
    private boolean alive = true;

    /** The type of the ball. */
    public BallType type;

    /** The pocket in which a ball is if any. -1 if not in any. */
    public int inPocket = -1;

    /** To prevent multiple checks on score update. */
    public boolean enteredPocket = false;
    
    /** The number of the ball. */
    public int number;

    /**
     * Instantiates a new ball.
     * 
     * @param ballType
     *            the ball type
     * @param bNumber
     *            the ball number
     * @param texture
     *            the texture
     * @param ballRadius
     *            the radius
     * @param x
     *            the x coordinate
     * @param y
     *            the y coordinate
     * @param velocity
     *            the speed
     */
    public Ball(final BallType ballType, final int bNumber,
            final Sphere texture, final float ballRadius, final float x,
            final float y, final Speed velocity) {
        sphere = texture;
        radius = ballRadius;
        xCoord = x;
        yCoord = y;
        prevX = xCoord;
        prevY = yCoord;
        speed = velocity;
        type = ballType;
        number = bNumber;
    }

    /**
     * Checks if is in pocket.
     * 
     * @param pocketNumber
     *            the pocket number
     */
    public final void isInPocket(final int pocketNumber) {
        if (zCount == 0) {
            alive = false;
            Log.w("loop", "INSIDE LOOP");
        }
        zCount--;
        zCoord -= zCount * FALL_VELOCITY;
        inPocket = pocketNumber;
    }

    /**
     * Checks if the ball is alive.
     * 
     * @return the boolean
     */
    public final Boolean isAlive() {
        return alive;
    }

    /**
     * Sets the alive in order to stop displaying the ball.
     */
    public final void setAlive() {
        alive = true;
        zCount = 360;
        zCoord = -48.0f;
        inPocket = -1;
    }

    /**
     * Checks if the ball is moving.
     * 
     * @return true, if it is moving
     */
    public final boolean isMoving() {
        if (speed.xVel != 0 || speed.yVel != 0) {
            return true;
        }
        return false;
    }
}
