package com.project.thirdyear.rkardjaliev.game;

import com.project.thirdyear.rkardjaliev.models.BallType;

/**
 * The Class Player. Contains player information like name
 * turn score and ball type.
 * 
 * @author Robert Kardjaliev
 */
public class Player {
	
	/** The name of the player. */
	private String name;
	
	/** The player's score. */
	private int score = 0;
	
	/** The ball type of the player. */
	private BallType ballType;
	
	/** Is it player's turn. */
	private boolean turn;
	
	/** Has player scored in this turn. */
	private boolean hasScored = false;
	
	/** Has player scored in this turn. */
	private boolean hasScoredWhite = false;
	
	/**
	 * Instantiates a new player.
	 *
	 * @param pName the name
	 */
	public Player(final String pName) {
		setName(pName);
	}

	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public final int getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 *
	 * @param mScore the new score
	 */
	public final void setScore(final int mScore) {
		this.score = mScore;
	}

	/**
	 * Gets the name of the player.
	 *
	 * @return the player's name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Sets the name of the player.
	 *
	 * @param pName the new name
	 */
	public final void setName(final String pName) {
		this.name = pName;
	}

	/**
	 * Gets the ball type.
	 *
	 * @return the ball type
	 */
	public final BallType getBallType() {
		return ballType;
	}

	/**
	 * Sets the ball type.
	 *
	 * @param bType the new ball type
	 */
	public final void setBallType(final BallType bType) {
		this.ballType = bType;
	}

	/**
	 * Gets the turn.
	 *
	 * @return the turn
	 */
	public final boolean getTurn() {
		return turn;
	}

	/**
	 * Sets the turn.
	 *
	 * @param pTurn the new turn
	 */
	public final void setTurn(final boolean pTurn) {
		this.turn = pTurn;
	}

	/**
	 * Returns whether the player has scored or not.
	 * @return
	 * 		the boolean value of whether player has scored
	 */
	public final boolean hasScored() {
		return hasScored;
	}

	/**
	 * Sets whether the player has scored or not.
	 * @param bool
	 * 			the boolean value of whether player has scored
	 */
	public final void setHasScored(final boolean bool) {
		this.hasScored = bool;
	}

	/**
	 * Returns whether the player has potted white ball or not.
	 * @return
	 * 		the boolean value of whether player has scored
	 */
	public final boolean hasScoredWhite() {
		return hasScoredWhite;
	}

	/**
	 * 	 * Sets whether the player has scored white ball or not.
	 * @param bool
	 * 		the boolean value of whether player has been potted
	 */
	public final void setHasScoredWhite(final boolean bool) {
		this.hasScoredWhite = bool;
	}
	
}
