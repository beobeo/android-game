package com.project.thirdyear.rkardjaliev.models;


/**
 * The class dealing with ball speeds.
 * 
 * @author Robert Kardjaliev
 */
public class Speed {

	/** The x velocity. */
	public float xVel = 0;

	/** The y velocity. */
	public float yVel = 0;

	/**
	 * Instantiates a new speed.
	 * 
	 * @param xVelocity
	 *            the x velocity
	 * @param yVelocity
	 *            the y velocity
	 */
	public Speed(final float xVelocity, final float yVelocity) {
		this.xVel = xVelocity;
		this.yVel = yVelocity;
	}

	/**
	 * Gets the velocity of a ball.
	 * 
	 * @param theta
	 *            the speed
	 * @return the component velocity
	 */
	public final float getComponent(final float theta) {
		return (float) Math.cos(theta) * xVel + (float) Math.sin(theta) * yVel;
	}

	/**
	 * Adds the per X/Y component velocity.
	 * 
	 * @param theta
	 *            the angle theta
	 * @param speed
	 *            the speed
	 */
	public final void addComponent(final float theta, final float speed) {
		xVel += (float) Math.cos(theta) * speed;
		yVel += (float) Math.sin(theta) * speed;
	}
}
