package com.project.thirdyear.rkardjaliev.models;

/**
 * The Class Collisions. Responsible for keeping track
 * of whether collisions have been checked in the current iteration.
 * @author Robert Kardjaliev
 */
public class Collisions {
	
	/** The 2D array responsible for keeping track
	 * of  collisions. */
	public int collisions[][];
	/** The size of the 2D array. */
	private int totalBalls;
	
	/**
	 * Instantiates a new collisions 2D array.
	 *
	 * @param num1 the size of the array.
	 */
	public Collisions(final int num1) {
		this.totalBalls = num1;
		collisions = new int [totalBalls][totalBalls];
		initCollisions();
	}
	
	/**
	 * Marks the collision between two balls as checked.
	 *
	 * @param num1 the first ball number
	 * @param num2 the second ball number
	 */
	public final void checkCollision(final int num1, final int num2) {
		collisions[num1][num2] = 1;
		collisions[num2][num1] = 1;
	}
	
	/**
	 * Collision checked.
	 *
	 * @param num1 the num1
	 * @param num2 the num2
	 * @return true, if successful
	 */
	public final boolean collisionChecked(final int num1, final int num2) {
		if (collisions[num1][num2] == 1 || collisions[num1][num2] == 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * Inits the collisions.
	 */
	public final void initCollisions() {
		for (int i = 0; i < totalBalls; i++) {
			for (int j = 0; j < totalBalls; j++) {
				collisions[i][j] = 0;
			}
		}
	}
}
