package com.project.thirdyear.rkardjaliev.graphics;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;
import android.os.SystemClock;
import android.util.Log;

import com.project.thirdyear.rkardjaliev.R;
import com.project.thirdyear.rkardjaliev.game.Physics;
import com.project.thirdyear.rkardjaliev.models.Ball;
import com.project.thirdyear.rkardjaliev.models.BallType;
import com.project.thirdyear.rkardjaliev.models.Square;

/**
 * Provides drawing instructions for a {@link android.opengl.GLSurfaceView
 * GLSurfaceView} object.
 * 
 * @author Robert Kardjaliev
 */
public class GLRenderer implements Renderer {
	/** The first run. */
	private boolean firstRun = true;
	/*
	 * uncomment these and the ones in onDraw() to get performance information
	 * int counter = 0; long time3 = 0;
	 */
	// PERSPECTIVE PROJECTION SETUP
	/** Perspective setup, field of view component. */
	private static final float FOVY = 45.0f;

	/** Perspective setup, near component. */
	private static final float Z_NEAR = 0.1f;

	/** Perspective setup, far component. */
	private static final float Z_FAR = 55.0f;

	/** Objects' distance on the screen. */
	private static final float OBJECT_DISTANCE = -50.0f;

	/** The aspect ratio between actual width and height of screen. */
	private float aspectRatio;

	/** The context. */
	private final Context mContext;

	/** ArrayList of {@link Ball balls} to be drawn. */
	private ArrayList<Ball> balls;

	/** An instance of the {@link Physics}. */
	private Physics physics;

	/** The table. */
	private Square table;

	/** The stick. */
	private Square stick;

	/**
	 * Constructor to set the handed over context.
	 * 
	 * @param context
	 *            The context.
	 */
	public GLRenderer(final Context context) {
		this.mContext = context;

		table = new Square();
		stick = new Square();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.opengl.GLSurfaceView.Renderer#onSurfaceCreated
	 * (javax.microedition.khronos.opengles.GL10,
	 * javax.microedition.khronos.egl.EGLConfig)
	 */
	@Override
	public final void onSurfaceCreated(final GL10 gl, final EGLConfig config) {
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glEnable(GL10.GL_CULL_FACE); // Enable cull face
		gl.glCullFace(GL10.GL_BACK); // Cull the back face (don't display)
		gl.glShadeModel(GL10.GL_SMOOTH);
		gl.glClearColor(0f, 0f, 0f, 1f); // black background
		gl.glClearDepthf(1.0f);
		gl.glEnable(GL10.GL_DEPTH_TEST);
		gl.glDepthFunc(GL10.GL_LEQUAL);
		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);

		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		table.loadGLTexture(gl, mContext, R.drawable.table);
		stick.loadGLTexture(gl, mContext, R.drawable.pool_cue_stick1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.opengl.GLSurfaceView.Renderer#onDrawFrame
	 * (javax.microedition.khronos.opengles.GL10)
	 */
	@Override
	public final void onDrawFrame(final GL10 gl) {
		// uncomment to get performance information
		//long time = SystemClock.elapsedRealtimeNanos();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		for (Ball b : balls) {
			physics.checkCollisions(b);

			gl.glLoadIdentity();
			physics.move(b);
			gl.glTranslatef(b.xCoord, b.yCoord, b.zCoord);

			if (b.isAlive()) {
				physics.spin(b);
				gl.glRotatef(90.0f, 0, 0, 1f);
				gl.glRotatef(b.xRotate, 1f, 0, 0);
				gl.glRotatef(b.yRotate, 0, 1f, 0);
				b.sphere.draw(gl);
			}
		}
		physics.updateBallList();
		gl.glLoadIdentity();
		gl.glTranslatef(0f, 0f, OBJECT_DISTANCE);
		gl.glScalef(37.2f, 16.6f, 0f);
		// gl.glRotatef(-90f, 0f, 0f, 1.0f);
		table.draw(gl);

		if (!physics.areMoving()) {
			if (physics.getStickEnabled()) {
				gl.glLoadIdentity();
				gl.glTranslatef(balls.get(0).xCoord, balls.get(0).yCoord,
						OBJECT_DISTANCE + 2f);
				gl.glRotatef(physics.getStickAngle(), 0, 0, 1.0f);
				gl.glScalef(24f, 1.5f, 0);

				stick.draw(gl);
			}
		}
		/* uncomment to get performance information
		 * long time2 = SystemClock.elapsedRealtimeNanos(); time3 += time2-time;
		 * Log.w("TIME", "The time " + (time2-time)); counter++;
		 * Log.w("AVERAGE", "Time " + time3/counter);(
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.opengl.GLSurfaceView.Renderer#onSurfaceChanged
	 * (javax.microedition.khronos.opengles.GL10, int, int)
	 */
	@Override
	public final void onSurfaceChanged(final GL10 gl, final int width,
			final int height) {
		aspectRatio = (float) width / (float) height;
		physics.setScalingFactors(width, height);
		if (firstRun) {
			firstRun = false;
			setBalls();
			loadBallTextures(gl);
		}

		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		GLU.gluPerspective(gl, FOVY, aspectRatio, Z_NEAR, Z_FAR);
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	/**
	 * Loads the ball textures.
	 * 
	 * @param gl
	 *            the {@link GL10} instance
	 */
	private void loadBallTextures(final GL10 gl) {
		// CHECKSTYLE:OFF
		balls.get(15).sphere
				.loadGLTexture(gl, this.mContext, R.drawable.ball15);
		balls.get(15).type = BallType.STRIPE;
		balls.get(1).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball5);
		balls.get(1).type = BallType.SOLID;
		balls.get(2).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball13);
		balls.get(2).type = BallType.STRIPE;
		balls.get(3).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball4);
		balls.get(3).type = BallType.SOLID;
		balls.get(4).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball2);
		balls.get(4).type = BallType.SOLID;
		balls.get(5).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball6);
		balls.get(5).type = BallType.SOLID;
		balls.get(6).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball7);
		balls.get(6).type = BallType.SOLID;
		balls.get(7).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball11);
		balls.get(7).type = BallType.STRIPE;
		balls.get(8).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball9);
		balls.get(8).type = BallType.STRIPE;
		balls.get(9).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball10);
		balls.get(9).type = BallType.STRIPE;
		balls.get(10).sphere
				.loadGLTexture(gl, this.mContext, R.drawable.ball12);
		balls.get(10).type = BallType.STRIPE;
		balls.get(11).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball8);
		balls.get(11).type = BallType.BLACK;
		balls.get(12).sphere
				.loadGLTexture(gl, this.mContext, R.drawable.ball14);
		balls.get(12).type = BallType.STRIPE;
		balls.get(13).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball3);
		balls.get(13).type = BallType.SOLID;
		balls.get(14).sphere.loadGLTexture(gl, this.mContext, R.drawable.ball1);
		balls.get(14).type = BallType.SOLID;
		balls.get(0).sphere
				.loadGLTexture(gl, this.mContext, R.drawable.cueball);
		// CHECKSTLYE:ON
	}

	/**
	 * Sets the balls.
	 */
	public final void setBalls() {
		balls = physics.getBalls();
	}

	/**
	 * Sets the physics.
	 * 
	 * @param mPhysics
	 *            the new physics
	 */
	public final void setPhysics(final Physics mPhysics) {
		this.physics = mPhysics;
	}
}
