package com.project.thirdyear.rkardjaliev.game;

/**
 * An enum type to depict specific game states.
 * 
 * @author Robert Kardjaliev
 *
 */
public enum GameState {
	
	/** Beginning of the game before balls are broken. */
	BEGIN,
	/** End of round. */
	ENDED,
	/** Start of round. */
	STARTED, 
	/** A solid colored ball. */
	OVER
}
