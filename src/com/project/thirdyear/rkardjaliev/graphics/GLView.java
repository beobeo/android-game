package com.project.thirdyear.rkardjaliev.graphics;

import com.project.thirdyear.rkardjaliev.game.Controller;
import com.project.thirdyear.rkardjaliev.game.Physics;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 
 * A {@link GLSurfaceView view} container to be used for capturing touch events
 * and drawing aiming system.
 * 
 * @author Robert Kardjaliev
 */
public class GLView extends GLSurfaceView {

	/** An instance of the {@link GLRenderer renderer}. */
	private final GLRenderer mRenderer;

	/** The X coordinate of a touch event. */
	private float xCoord;
	/** The y coordinate of a touch event. */
	private float yCoord;

	/** The previous xCoord coordinate. */
	private float prevX;
	/** The previous yCoord coordinate. */
	private float prevY;
	
	/** 
	 * The difference between the current and the previous
	 * touch events' X coordinates.
	 */
	private float deltaX;
	/** 
	 * The difference between the current and the previous
	 * touch events' Y coordinates.
	 */
	private float deltaY;
	/** A scale factor to lower rotation angle of cue stick. */
	private static final float SCALE_FACTOR = 0.2f;

	/** The physics. */
	private Physics physics;

	/**
	 * The object toast used for displaying short messages to the user.
	 */
	private UserMessage toast;

	/**
	 * An instance of the controller.
	 */
	private Controller controller;

	/**
	 * Boolean containing the value of whether user is moving the white ball.
	 */
	private boolean movingWhite = false;

	/**
	 * Instantiates a new {@link GLView GL Surface GLView}.
	 * 
	 * @param context
	 *            the current application context
	 * @param attrs
	 *            XML attributes for parsing
	 */
	public GLView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		toast = new UserMessage(context);
		mRenderer = new GLRenderer(context);
		setRenderer(mRenderer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public final boolean onTouchEvent(final MotionEvent e) {
		xCoord = e.getX();
		yCoord = e.getY();
		//Log.w("Touch", "xCoord " + xCoord + "yCoord " + yCoord);
		switch (e.getAction()) {		
		case MotionEvent.ACTION_MOVE:
			physics.screenToWorld(xCoord, yCoord);
			if (movingWhite) {
					physics.setWhite();
			} else {
			deltaX = xCoord - prevX;			
			deltaY = yCoord - prevY;

			// reverse direction for proper cue stick rotation
			deltaY = deltaY * -1;

			if (physics.touchY > physics.white.yCoord) {
				// reverse if above white ball
				deltaX = deltaX * -1;
			}

			if (physics.touchX < physics.white.xCoord) { 
				// reverse if on left of white ball
				deltaY = deltaY * -1;
			}

			physics.setAimAngle(physics.getStickAngle()
					+ ((deltaX + deltaY) * SCALE_FACTOR));
			}
			break;
			
		case MotionEvent.ACTION_DOWN:
			if (controller.isResetWhite()) {
				physics.screenToWorld(xCoord, yCoord);
				if (physics.whiteSelected()) {
					movingWhite = true;
				}
			}
			break;
		case MotionEvent.ACTION_UP:
			movingWhite = false;
			physics.enableStick(true);
			break;
		case MotionEvent.ACTION_CANCEL:
			movingWhite = false;
			break;
		default:
			movingWhite = false;
			break;
		}

		prevX = xCoord;
		prevY = yCoord;
		return true;
	}

	/**
	 * Sets a String to display via Toast message on screen.
	 * 
	 * @param s
	 *            the String to be displayed
	 */
	public final void setMessage(final String s) {
		toast.setMessage(s);
	}

	/**
	 * Sets the Physics engine.
	 * 
	 * @param thePhysics
	 *            the new physics
	 * @param mController
	 * 			  the controller
	 */
	public final void setMVC(final Controller mController,
			final Physics thePhysics) {
		this.physics = thePhysics;
		mRenderer.setPhysics(physics);
		this.controller = mController;
	}
}
