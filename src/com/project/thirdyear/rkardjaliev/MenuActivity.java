package com.project.thirdyear.rkardjaliev;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

/**
 * This is the Menu Activity class for the app. It allows
 * for user to exit application, select options or start
 * a new game.
 * @author Robert Kardjaliev
 */
public class MenuActivity extends Activity {

	/**
	 * Called when the activity is first created.
	 * 
	 * @param savedInstanceState
	 *            The instance state.
	 */
	@Override
	public final void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.menu);
	}

	/**
	 * Starts the new game activity.
	 * 
	 * @param view
	 *            the button view that was clicked
	 */
	public final void newGame(final View view) {
		Intent game = new Intent("com.project.thirdyear.rkardjaliev.NewGame");
		startActivity(game);
	}
	
	/**
	 * Starts the credits activity.
	 * 
	 * @param view
	 *            the button view clicked
	 */
	public final void credits(final View view) {
		Intent credits = new Intent(
				"com.project.thirdyear.rkardjaliev.Credits");
		startActivity(credits);
	}


	/**
	 * Quit and close the game.
	 * 
	 * @param view
	 *            the view the button was pressed from
	 */
	public final void quit(final View view) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
		alertDialogBuilder.setTitle("Stop the Application?");
		alertDialogBuilder
		.setMessage("Click yes to exit!")
		.setCancelable(false)
		.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {
				moveTaskToBack(true);
				android.os.Process
				.killProcess(android.os.Process.myPid());
				finish();
			}
		})

		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {

				dialog.cancel();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}
}
