package com.project.thirdyear.rkardjaliev.models;

/**
 * An enum type to depict specific ball types.
 * 
 * @author Robert Kardjaliev
 *
 */
public enum BallType {

	/** The white ball. */
	WHITE, 
	/** The black/EIGHT ball. */
	BLACK, 
	/** A solid colored ball. */
	SOLID, 
	/** A stripe colored ball. */
	STRIPE, 
	/** A ball without a color. */
	NONE
}
