package com.project.thirdyear.rkardjaliev;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

/**
 * This is the Main Activity class for the app. It is the main entry
 * point of the game. It displays the splash screen and starts the menu
 * Activity. Gets called once per application. You cannot go back to it
 * from the Menu activtiy.
 * 
 * @author Robert Kardjaliev
 */
public class MainActivity extends Activity {

    /** Amount of time the splash screen will remain active. */
    private static final int SLEEP_TIME = 5000;
    /** The message tag in Eclipse Log. */
    private static final String LOG = "SplashActivity";

    /**
     * Called when the activity is first created.
     * 
     * @param savedInstanceState
     *            The instance state.
     */
    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);
        Thread splashTimer = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(SLEEP_TIME);
                    Intent menu = new Intent(
                            "com.project.thirdyear.rkardjaliev.MENU");
                    menu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(menu);
                } catch (InterruptedException e) {
                    Log.w(LOG, "error " + e);
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        splashTimer.start();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onResume()
     */
    @Override
    protected final void onResume() {
        super.onResume();
    }

    /*
     * (non-Javadoc)
     * 
     * @see android.app.Activity#onPause()
     */
    @Override
    protected final void onPause() {
        super.onPause();
    }
}
